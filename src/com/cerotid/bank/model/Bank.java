package com.cerotid.bank.model;

import java.util.ArrayList;

public class Bank {
	// TODO
	private String bankName;
	private ArrayList<Customer> customers;

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	void printBankName() {
		System.out.println(bankName);
		
	}

	void printBankDetails() {

		System.out.println(toString());
		

	}

	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}
	

}
