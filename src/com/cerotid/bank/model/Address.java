package com.cerotid.bank.model;

public class Address {
	// TODO
	private String streetname;

	private String city;
	private String stateCode;
	private String zipCode;

	public Address(String streetname, String city, String stateCode, String zipCode) {
		this.streetname = streetname;
		this.zipCode = zipCode;
		this.city = city;
		this.stateCode = stateCode;
	}

	public String getStreetname() {
		return streetname;
	}

	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	@Override
	public String toString() {
		return "Address [streetname=" + streetname + ", city=" + city + ", stateCode=" + stateCode + ", zipCode="
				+ zipCode + "]";
	}

}
