package com.cerotid.bank.model;

import java.util.Date;

public class Account {
	
	private AccountType accountType;
	private Date accountOpenDate;
	private Date accountCloseDate;
	private double amount;
	

	
	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public Date getAccountCloseDate() {
		return accountCloseDate;
	}

	public void setAccountCloseDate(Date accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public void printAccountInfo() {
		System.out.println(toString());

	}
	
	public void sendMoney() {
		
	}

	@Override
	public String toString() {
		return "Account [accountType=" + accountType + ", accountOpenDate=" + accountOpenDate + ", accountCloseDate="
				+ accountCloseDate + ", amount=" + amount + "]";
	}

	
	

}
