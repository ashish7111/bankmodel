package com.cerotid.bank.model;

public enum DeliveryOptions {
	
	TEN("10 Minutes"),
	DAY("24 Hours");
	
	private String delOpt;
	
	DeliveryOptions(String delOpt){
		this.delOpt = delOpt;
	}
	
	public String getDelOpt() {
		return delOpt;
	}

}
