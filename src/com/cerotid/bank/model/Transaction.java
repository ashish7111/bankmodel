package com.cerotid.bank.model;

public class Transaction {
	private String receiverFirstName;
	private String receiverLastName;
	private double amount;
	private double fee;

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void createTransaction() {
		System.out.println("Transaction Created");
	}

	public void deductAccountBalance() {
		System.out.println("Balance Deducted");
	}
	
	

}
