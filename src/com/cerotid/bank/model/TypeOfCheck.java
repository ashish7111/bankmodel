package com.cerotid.bank.model;

public enum TypeOfCheck {
	
	PAPER("Paper Check"),
	E("E-Check");
	
	private String typCheck;
	
	TypeOfCheck (String typCheck){
		this.typCheck = typCheck;
	}
	
	public String getTypCheck() {
		return typCheck;
	}

}
